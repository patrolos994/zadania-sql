--2.a
SELECT ROUND(AVG(age), 2) AS srednia_wiek 
FROM batman 
WHERE age IS NOT NULL;

--2.b
SELECT ABS(-3) AS wartosc_bezwzgledna;

--2.c
SELECT 2*3 wynik_mnozenia;

--2.d
SELECT 6/2 AS wynik_dzielenia;