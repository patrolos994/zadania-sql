--1.
CREATE DATABASE Baza_Pracownicy;

USE Baza_Pracownicy;

--2.
CREATE TABLE IF NOT EXISTS Pracownicy (
    ID INT PRIMARY KEY,
    Imię VARCHAR(50) NOT NULL,
    Nazwisko VARCHAR(50) NOT NULL,
    Wiek INT,
    Kurs VARCHAR(100)
);

--3.
INSERT INTO Pracownicy (ID, Imię, Nazwisko, Wiek, Kurs)
VALUES
(1, 'Anna', 'NOWAK', 34, 'DS'),
(2, 'Roman', 'KOWALSKI', 42, 'DS'),
(3, 'Tomasz', 'WIŚNIEWSKI', 33, 'DS'),
(4, 'Agata', 'WÓJCIK', 43, 'DS'),
(5, 'Elżbieta', 'KOWALCZYK', 28, 'Java'),
(6, 'Przemysław', 'KAMIŃSKI', 34, 'Java'),
(7, 'Robert', 'LEWANDOWSKI', 35, 'Java'),
(8, 'Radosław', 'ZIELIŃSKI', 38, 'Java'),
(9, 'Anna', 'WOŹNIAK', 26, 'Java'),
(10, 'Robert', 'SZYMAŃSKI', 34, 'Java'),
(11, 'Radosław', 'DĄBROWSKI', 35, 'UX'),
(12, 'Robert', 'KOZŁOWSKI', 38, 'UX'),
(13, 'Joanna', 'MAZUR', 26, 'UX'),
(14, 'Radosław', 'JANKOWSKI', 27, 'UX'),
(15, 'Patryk', 'LEWANDOWSKI', 28, 'Tester'),
(16, 'Patryk', 'ZIELIŃSKI', 28, 'Tester'),
(17, 'Andrzej', 'WOŹNIAK', 31, 'Tester'),
(18, 'Andrzej', 'LEWANDOWSKI', 30, 'Tester'),
(19, 'Roman', 'ZIELIŃSKI', 39, 'Tester'),
(20, 'Ewa', 'WOŹNIAK', 31, 'Tester');

--4.
SELECT * FROM Pracownicy WHERE Wiek = 28;

--5.
SELECT * FROM Pracownicy WHERE Wiek <= 30;

--6.
SELECT * FROM Pracownicy WHERE Nazwisko LIKE '%ski';

--7.
SELECT * FROM Pracownicy WHERE ID IN (1,4,7,18,20);

--8.
SELECT * FROM Pracownicy WHERE Imię IS NULL OR Nazwisko IS NULL OR Wiek IS NULL OR Kurs IS NULL;

--9.
SELECT * FROM Pracownicy WHERE kurs <> 'DS';
