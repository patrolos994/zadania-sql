--1.
CREATE DATABASE IF NOT EXISTS Pracownicy;
USE Pracownicy;

--2.
CREATE TABLE IF NOT EXISTS Pracownicy (
    ID INT PRIMARY KEY,
    Imię VARCHAR(50) NOT NULL,
    Nazwisko VARCHAR(50) NOT NULL,
    Wiek INT,
    Kurs VARCHAR(100)
);

--3.
INSERT INTO Pracownicy (ID, Imię, Nazwisko, Wiek, Kurs)
VALUES
(1, 'Anna', 'NOWAK', 34, 'DS'),
(2, 'Roman', 'KOWALSKI', 42, 'DS'),
(3, 'Tomasz', 'WIŚNIEWSKI', 33, 'DS'),
(4, 'Agata', 'WÓJCIK', 43, 'DS'),
(5, 'Elżbieta', 'KOWALCZYK', 28, 'Java'),
(6, 'Przemysław', 'KAMIŃSKI', 34, 'Java'),
(7, 'Robert', 'LEWANDOWSKI', 35, 'Java'),
(8, 'Radosław', 'ZIELIŃSKI', 38, 'Java'),
(9, 'Anna', 'WOŹNIAK', 26, 'Java'),
(10, 'Robert', 'SZYMAŃSKI', 34, 'Java'),
(11, 'Radosław', 'DĄBROWSKI', 35, 'UX'),
(12, 'Robert', 'KOZŁOWSKI', 38, 'UX'),
(13, 'Joanna', 'MAZUR', 26, 'UX'),
(14, 'Radosław', 'JANKOWSKI', 27, 'UX'),
(15, 'Patryk', 'LEWANDOWSKI', 28, 'Tester'),
(16, 'Patryk', 'ZIELIŃSKI', 28, 'Tester'),
(17, 'Andrzej', 'WOŹNIAK', 31, 'Tester'),
(18, 'Andrzej', 'LEWANDOWSKI', 30, 'Tester'),
(19, 'Roman', 'ZIELIŃSKI', 39, 'Tester'),
(20, 'Ewa', 'WOŹNIAK', 31, 'Tester');

--4.
SELECT DISTINCT Imię FROM Pracownicy;

--5.
SELECT DISTINCT Nazwisko FROM Pracownicy;

--6.
SELECT DISTINCT Szkolenie FROM Pracownicy WHERE Nazwisko = 'Kowalczyk';

--7.
SELECT * FROM Pracownicy WHERE Wiek IS NULL;

--8.
SELECT Wiek FROM Pracownicy WHERE Imię = 'Patryk';

--9.
ALTER TABLE Pracownicy RENAME TO Mentorzy;