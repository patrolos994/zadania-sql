--1.a
SELECT COUNT(ID) AS liczba_rekordow
FROM batman;

--1.b
SELECT COUNT(age) AS Wiek
FROM batman;

--1.c
SELECT COUNT(*) AS Faceci_Powyżej_40lat
FROM batman
WHERE sex = 'M' AND age > 40;

--1.d
SELECT SUM(price) AS suma_cen
FROM batman;

--1.e
SELECT SUM(age) AS laczyn_wiek_kobiet
FROM batman
WHERE sex = 'F'; 

--1.f
SELECT SUM(price) AS laczna_wartosc
FROM batman
WHERE gift IN ('komputer', 'okno');

--1.g
SELECT MAX(price) AS maksymalna_cena 
FROM batman;

--1.h
SELECT MAX(start_date) AS Najpozniejsza_data
FROM batman;

--1.i
SELECT last_name 
FROM batman 
WHERE last_name >= '%X' 
ORDER BY last_name 
LIMIT 1;

--1.j
SELECT MIN(price) AS minimalna_cena
FROM batman;

--1.k
SELECT MIN(start_date) AS Najwczesniejsza_data
FROM batman;

--1.l
SELECT last_name 
FROM batman 
WHERE last_name >= 'A%' 
ORDER BY last_name 
LIMIT 1;

--1.m
SELECT MIN(price) AS Minimalna_cena,
       MAX(price) AS Maxymalna_cena
FROM batman;

--1.n
SELECT (MAX(price) - MIN(price)) AS roznica_cen 
FROM batman;

--1.o
SELECT ROUND(AVG(age)) AS Srednia_wieku
FROM batman;

--1.p
SELECT ROUND(SUM(age) / COUNT(age)) AS Srednia_wieku
FROM batman;

--1.q
SELECT 
    ROUND(AVG(CASE WHEN sex = 'F' THEN age ELSE NULL END)) AS sredni_wiek_kobiet,
    ROUND(AVG(CASE WHEN sex = 'M' THEN age ELSE NULL END)) AS sredni_wiek_mezczyzn
FROM batman 
WHERE age IS NOT NULL;
