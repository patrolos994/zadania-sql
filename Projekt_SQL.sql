--1.
CREATE DATABASE Sklep_odziezowy;
USE Sklep_odziezowy;

--2.
CREATE TABLE Producenci (
    id_producenta INT PRIMARY KEY,
    nazwa_producenta VARCHAR(255) NOT NULL,
    adres_producenta VARCHAR(255) NOT NULL,
    nip_producenta VARCHAR(15) NOT NULL,
    data_podpisania_umowy DATE NOT NULL
);

--3.
CREATE TABLE Produkty (
    id_produktu INT PRIMARY KEY,
    id_producenta INT,
    nazwa_produktu VARCHAR(255) NOT NULL,
    opis_produktu TEXT,
    cena_netto_zakupu DECIMAL(10, 2) NOT NULL,
    cena_brutto_zakupu DECIMAL(10, 2) NOT NULL,
    cena_netto_sprzedaży DECIMAL(10, 2) NOT NULL,
    cena_brutto_sprzedaży DECIMAL(10, 2) NOT NULL,
    procent_VAT_sprzedaży DECIMAL(5, 2) NOT NULL
);

--4.
CREATE TABLE Zamówienia (
    id_zamówienia INT PRIMARY KEY,
    id_klienta INT,
    id_produktu INT,
    data_zamówienia DATE NOT NULL
);

--5.
CREATE TABLE Klienci (
    id_klienta INT PRIMARY KEY,
    imię VARCHAR(50) NOT NULL,
    nazwisko VARCHAR(50) NOT NULL,
    adres VARCHAR(255) NOT NULL
);

--6.
ALTER TABLE Produkty
ADD CONSTRAINT fk_producenta
FOREIGN KEY (id_producenta) 
REFERENCES Producenci(id_producenta);

ALTER TABLE Zamówienia
ADD CONSTRAINT fk_produktu
FOREIGN KEY (id_produktu) 
REFERENCES Produkty(id_produktu);


ALTER TABLE Zamówienia
ADD CONSTRAINT fk_klienta
FOREIGN KEY (id_klienta) 
REFERENCES Klienci(id_klienta);

--7.
INSERT INTO Producenci (id_producenta, nazwa_producenta, adres_producenta, nip_producenta, data_podpisania_umowy)
VALUES
(1, 'Nike', 'ul. Nike 1, Warszawa', '1234567890', '2023-01-01'),
(2, 'Adidas', 'ul. Adidas 2, Kraków', '0987654321', '2023-02-15'),
(3, 'Puma', 'ul. Puma 3, Gdańsk', '1357924680', '2023-03-30'),
(4, 'Reebok', 'ul. Reebok 4, Poznań', '2468135790', '2023-04-20');

INSERT INTO Produkty (id_produktu, id_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedaży, cena_brutto_sprzedaży, procent_VAT_sprzedaży)
VALUES
(1, 1, 'Buty sportowe Nike Air Max', 'Buty sportowe Nike Air Max w kolorze czarnym', 150.00, 184.50, 250.00, 307.50, 23.00),
(2, 1, 'Koszulka Nike Dry-FIT', 'Koszulka treningowa Nike Dry-FIT w kolorze niebieskim', 30.00, 36.90, 60.00, 73.80, 23.00),
(3, 2, 'Buty do biegania Adidas UltraBoost', 'Buty do biegania Adidas UltraBoost w kolorze szarym', 140.00, 172.20, 230.00, 283.90, 23.00),
(4, 2, 'Spodnie dresowe Adidas', 'Spodnie dresowe Adidas w kolorze czarnym', 50.00, 61.50, 100.00, 123.00, 23.00),
(5, 3, 'Koszulka Puma Essential', 'Koszulka Puma Essential w kolorze czerwonym', 25.00, 30.75, 50.00, 61.50, 23.00),
(6, 3, 'Buty do biegania Puma Speed 600', 'Buty do biegania Puma Speed 600 w kolorze niebieskim', 120.00, 147.60, 200.00, 246.00, 23.00),
(7, 4, 'Buty sportowe Reebok Nano X', 'Buty sportowe Reebok Nano X w kolorze czarnym', 130.00, 159.90, 220.00, 270.60, 23.00),
(8, 4, 'Kurtka Reebok Windbreaker', 'Kurtka Reebok Windbreaker w kolorze czarnym', 80.00, 98.40, 160.00, 196.80, 23.00),
(9, 1, 'Szkarpety Nike Everyday Plus Cushioned', 'Szkarpety Nike Everyday Plus Cushioned w kolorze białym', 10.00, 12.30, 20.00, 24.60, 23.00),
(10, 2, 'Koszulka Adidas Graphic', 'Koszulka Adidas Graphic w kolorze zielonym', 35.00, 43.05, 70.00, 86.10, 23.00),
(11, 3, 'Buty do biegania Puma NRGY Comet', 'Buty do biegania Puma NRGY Comet w kolorze różowym', 80.00, 98.40, 160.00, 196.80, 23.00),
(12, 4, 'Spodnie dresowe Reebok Linear Logo', 'Spodnie dresowe Reebok Linear Logo w kolorze niebieskim', 45.00, 55.35, 90.00, 110.70, 23.00),
(13, 1, 'Kurtka Nike Sportswear Windrunner', 'Kurtka Nike Sportswear Windrunner w kolorze czerwonym', 90.00, 110.70, 180.00, 222.60, 23.00),
(14, 2, 'Spodenki Adidas 3-Stripes', 'Spodenki Adidas 3-Stripes w kolorze szarym', 25.00, 30.75, 50.00, 61.50, 23.00),
(15, 3, 'Koszulka Puma Big Cat', 'Koszulka Puma Big Cat w kolorze czarnym', 20.00, 24.60, 40.00, 49.20, 23.00),
(16, 4, 'Buty sportowe Reebok Classic Leather', 'Buty sportowe Reebok Classic Leather w kolorze białym', 70.00, 86.10, 140.00, 172.20, 23.00),
(17, 1, 'Kurtka Nike Academy 21', 'Kurtka Nike Academy 21 w kolorze czarnym', 80.00, 98.40, 160.00, 196.80, 23.00),
(18, 2, 'Spodenki Adidas Tiro 21', 'Spodenki Adidas Tiro 21 w kolorze czerwonym', 30.00, 36.90, 60.00, 73.80, 23.00),
(19, 3, 'Buty do biegania Puma Liberate Nitro', 'Buty do biegania Puma Liberate Nitro w kolorze czarnym', 130.00, 159.90, 220.00, 270.60, 23.00),
(20, 4, 'Koszulka Reebok United by Fitness', 'Koszulka Reebok United by Fitness w kolorze szarym', 20.00, 24.60, 40.00, 49.20, 23.00);

INSERT INTO Klienci (id_klienta, imię, nazwisko, adres)
VALUES
(1, 'Adam', 'Nowak', 'ul. Kwiatowa 1, Warszawa'),
(2, 'Ewa', 'Kowalska', 'ul. Słoneczna 2, Kraków'),
(3, 'Marek', 'Wiśniewski', 'ul. Leśna 3, Gdynia'),
(4, 'Karolina', 'Kamińska', 'ul. Polna 4, Poznań'),
(5, 'Piotr', 'Lewandowski', 'ul. Ogrodowa 5, Wrocław'),
(6, 'Aleksandra', 'Nowak', 'ul. Lipowa 6, Lublin'),
(7, 'Paweł', 'Woźniak', 'ul. Słonecznikowa 7, Szczecin'),
(8, 'Natalia', 'Dąbrowska', 'ul. Brzozowa 8, Białystok'),
(9, 'Michał', 'Kaczmarek', 'ul. Różana 9, Radom'),
(10, 'Magdalena', 'Wojciechowska', 'ul. Kwiatowa 10, Katowice');

INSERT INTO Zamówienia (id_zamówienia, id_klienta, id_produktu, data_zamówienia)
VALUES
(1, 1, 3, '2024-04-01'),
(2, 2, 8, '2024-04-03'),
(3, 3, 12, '2024-04-05'),
(4, 4, 17, '2024-04-07'),
(5, 5, 4, '2024-04-09'),
(6, 6, 9, '2024-04-11'),
(7, 7, 14, '2024-04-13'),
(8, 8, 19, '2024-04-15'),
(9, 9, 6, '2024-04-17'),
(10, 10, 10, '2024-04-19');

--8.
SELECT p.*
FROM Produkty p
JOIN Producenci pr ON p.id_producenta = pr.id_producenta
WHERE pr.id_producenta = 1;

--9.
SELECT p.*
FROM Produkty p
JOIN Producenci pr ON p.id_producenta = pr.id_producenta
WHERE pr.id_producenta = 1
ORDER BY nazwa_produktu;

--10.
SELECT AVG(cena_brutto_sprzedaży) AS średnia_cena
FROM Produkty
WHERE id_producenta = 1;

--11.
SELECT id_produktu, id_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedaży, cena_brutto_sprzedaży, procent_VAT_sprzedaży,
    CASE 
        WHEN cena_brutto_sprzedaży <= (
            SELECT 
                AVG(cena_brutto_sprzedaży)
            FROM 
                (SELECT cena_brutto_sprzedaży, NTILE(2) OVER (ORDER BY cena_brutto_sprzedaży) AS Kwartyle FROM produkty WHERE id_producenta = 1) AS Kwartyl
            WHERE 
                Kwartyle = 1
        ) THEN 'Tanie'
        ELSE 'Drogie'
    END AS grupa_cenowa
FROM produkty
WHERE id_producenta = 1
ORDER BY nazwa_produktu;

--12.
SELECT DISTINCT p.nazwa_produktu
FROM Produkty p
JOIN Zamówienia z  ON p.id_produktu = z.id_produktu;

--13.
SELECT p. id_produktu, id_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedaży, cena_brutto_sprzedaży, procent_VAT_sprzedaży
FROM Produkty p
JOIN Zamówienia z ON p.id_produktu = z.id_produktu
LIMIT 5;

--14.
SELECT SUM(cena_brutto_sprzedaży) AS łączna_wartość_zamówień
FROM Zamówienia
JOIN Produkty ON Zamówienia.id_produktu = Produkty.id_produktu;

--15.
SELECT z.id_zamówienia, p.nazwa_produktu, z.data_zamówienia
FROM Zamówienia z
JOIN Produkty p ON z.id_produktu = p.id_produktu
ORDER BY z.data_zamówienia ASC;

--16.
SELECT *
FROM Produkty
WHERE id_producenta IS NULL OR nazwa_produktu IS NULL OR opis_produktu IS NULL OR cena_netto_zakupu IS NULL OR cena_brutto_zakupu IS NULL OR cena_netto_sprzedaży IS NULL OR cena_brutto_sprzedaży IS NULL OR procent_VAT_sprzedaży IS NULL;

--17.
SELECT p.nazwa_produktu, COUNT(*) AS liczba_zamówień
FROM Produkty p
JOIN Zamówienia z ON p.id_produktu = z.id_produktu
GROUP BY p.nazwa_produktu
ORDER BY COUNT(*) DESC
LIMIT 1;

--18.
SELECT data_zamówienia, COUNT(*) AS liczba_zamówień
FROM Zamówienia
GROUP BY data_zamówienia
ORDER BY COUNT(*) DESC
LIMIT 1;