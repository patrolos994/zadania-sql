--3.a
SELECT CONCAT(first_name, ' ', last_name) AS Pracownik 
FROM batman;

--3.b
SELECT CONCAT(gift, ' - cena: ', price) AS cennik 
FROM batman;

--3.c
SELECT CONCAT(first_name, ' ', last_name, ' ',gift, ' - cena: ', price) AS cennik 
FROM batman;

--3.d
SELECT UPPER(gift) AS Prezenty
FROM batman;

--3.e
SELECT LOWER(first_name) AS Imiona
FROM batman;

--3.f
SELECT last_name, LENGTH(last_name) AS Dlugosc
FROM batman;

--3.g
SELECT LEFT (last_name, 2) AS Dwie_litery
FROM batman;

--3.h
SELECT CONCAT(LOWER(LEFT(first_name, 1)), LOWER(LEFT(last_name, 3))) AS login 
FROM batman;
