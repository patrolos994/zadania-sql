--4.a
SELECT YEAR(start_date) AS ROK, MONTH(start_date) AS Miesiac, DAY(start_date) AS Dzien
FROM batman;

--4.b
SELECT start_date, DATE_ADD(start_date, INTERVAL 3 DAY) AS final_date 
FROM batman;

--4.c
SELECT CURDATE() AS Dzisiejsza_data

--4.d
SELECT MONTHNAME(CURDATE()) AS nazwa_miesiaca;

--4.e
SELECT 
    start_date,
    WEEK(start_date) AS numer_tygodnia,
    MONTHNAME(start_date) AS nazwa_miesiaca,
    QUARTER(start_date) AS kwartal,
    DAYOFYEAR(start_date) AS numer_dnia_w_roku
FROM batman;