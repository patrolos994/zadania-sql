--WPROWADZENIE.A.
SELECT customer.CUST_NAME 
FROM customer
JOIN orders ON customer.CUST_CODE = orders.CUST_CODE
WHERE orders.ORD_DATE = '2008-01-08';

--B.
SELECT agents.AGENT_NAME, customer.CUST_NAME, agents.WORKING_AREA
FROM agents
JOIN customer ON agents.AGENT_CODE = customer.AGENT_CODE;

--INNER JOIN.A.
SELECT customer.CUST_NAME, agents.AGENT_NAME AS Salesman
FROM customer
INNER JOIN agents ON customer.AGENT_CODE = agents.AGENT_CODE
WHERE agents.COMMISSION > 12; // Tutaj jest w zadaniu 12% a w columnie mają 0.11% i zastanawiam się czy musiałem dać tak jak mają w columnie.

--B.
SELECT orders.ORD_NUM, orders.ORD_DATE, orders.ORD_AMOUNT, customer.CUST_NAME AS 'Customer Name', agents.AGENT_NAME AS Salesman, agents.COMMISSION
FROM orders
INNER JOIN customer ON orders.CUST_CODE = customer.CUST_CODE
INNER JOIN agents ON orders.AGENT_CODE = agents.AGENT_CODE;

--LEFT JOIN.A.
SELECT customer.CUST_CODE, customer.CUST_NAME, agents.AGENT_CODE, agents.AGENT_NAME AS Salesman, agents.WORKING_AREA
FROM customer
LEFT JOIN agents ON customer.AGENT_CODE = agents.AGENT_CODE
ORDER BY customer.CUST_CODE;

--RIGHT JOIN.A.
SELECT agents.AGENT_CODE, agents.AGENT_NAME AS Salesman, agents.WORKING_AREA
FROM agents
RIGHT JOIN customer ON agents.AGENT_CODE = customer.AGENT_CODE
ORDER BY agents.AGENT_CODE;

--CROSS JOIN.A.
SELECT agents.AGENT_NAME, customer.CUST_NAME
FROM agents
CROSS JOIN customer;

--B.
SELECT agents.AGENT_NAME, customer.CUST_NAME
FROM agents
CROSS JOIN customer
WHERE agents.WORKING_AREA = customer.WORKING_AREA;

--C.
SELECT agents.AGENT_NAME, customer.CUST_NAME
FROM agents
CROSS JOIN customer
WHERE agents.WORKING_AREA <> customer.WORKING_AREA AND customer.GRADE IS NOT NULL;

--UNION.A.
SELECT AGENT_CODE AS ID, AGENT_NAME, 'Salesman' AS Rodzaj
FROM agents
WHERE WORKING_AREA = 'London'
UNION
SELECT CUST_CODE AS ID, CUST_NAME, 'Customer' AS Rodzaj
FROM customer
WHERE CUST_CITY = 'London';

--B.
SELECT order_date, agent_name, MAX(order_amount) AS max_order_amount, MIN(order_amount) AS min_order_amount 
FROM (SELECT o1.ORD_DATE AS order_date, a.AGENT_NAME, o1.ORD_AMOUNT AS order_amount FROM orders o1 JOIN agents a ON o1.AGENT_CODE = a.AGENT_CODE 
    UNION ALL
    SELECT o2.ORD_DATE AS order_date, a.AGENT_NAME, o2.ORD_AMOUNT AS order_amount 
    FROM daysorder o2
    JOIN agents a ON o2.AGENT_CODE = a.AGENT_CODE) AS subquery 
GROUP BY order_date, agent_name 
ORDER BY order_date;

// Tutaj używam aliasów o1 i o2 dla tabeli orders i daysorder.
// Oraz używam  aliasu order_date w obu podzapytaniach, aby zwrócić datę zamówienia z obu tabel.

--WITH.A.
WITH londonstaff AS (
    SELECT *
    FROM agents
    WHERE WORKING_AREA = 'London' AND COMMISSION > 0.14
)
SELECT * FROM londonstaff;

--B.
WITH gradecount AS (
    SELECT GRADE, COUNT(*) AS number_of_customers
    FROM customer
    GROUP BY GRADE
)
SELECT * FROM gradecount;

--C.
WITH total_per_dzien AS (
    SELECT
        ORD_DATE AS ord_date,
        COUNT(DISTINCT CUST_CODE) AS number_of_customers,
        AVG(ORD_AMOUNT) AS avg_order_amount,
        SUM(ORD_AMOUNT) AS total_order_amount
    FROM orders
    GROUP BY ORD_DATE
)
SELECT * FROM total_per_dzien;
